package com.solutions.publixtest.network

open class Repository constructor( private val api: Api
): SafeApiRequest() {
    suspend fun getResults(format: String) = apiRequest {
        api.getResults(format)
    }

}