package com.solutions.publixtest.network

import com.solutions.publixtest.model.MainResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("launch/")
    suspend fun getResults(@Query("format") format: String ) : Response<MainResponse>

    companion object{
        operator fun invoke(): Api {
            return Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://ll.thespacedevs.com/2.2.0/").build().create(Api::class.java)
        }
    }
}