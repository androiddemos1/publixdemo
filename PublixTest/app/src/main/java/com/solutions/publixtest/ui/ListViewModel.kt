package com.solutions.publixtest.ui

import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.solution.tmbdemo.util.Coroutines
import com.solutions.publixtest.model.Result
import com.solutions.publixtest.network.Api
import com.solutions.publixtest.network.Repository
import kotlinx.coroutines.Job

class ListViewModel() : ViewModel() {

    private val _results = MutableLiveData<List<Result>>()
    val results:LiveData<List<Result>>get() { return _results}
    private lateinit var job: Job
    val api = Api()
    val repository = Repository(api)

        fun getResults() {

                job = Coroutines.ioThenMain(
                    {
                        repository.getResults("json")
                    },
                    {
                        if (it != null) {
                            _results.value = it.results
                        }
                    }
                )

        }

        override fun onCleared() {
            super.onCleared()
            if(::job.isInitialized) job.cancel()
        }

}