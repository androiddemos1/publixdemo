package com.solutions.publixtest.model

data class Status(
    val abbrev: String,
    val description: String,
    val id: Int,
    val name: String
)