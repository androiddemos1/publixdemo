package com.solutions.publixtest.model

data class Result(
    val failreason: Any,
    val hashtag: Any,
    val holdreason: Any,
    val id: String,
    val image: String,
    val infographic: Any,
    val last_updated: String,
    val launch_service_provider: LaunchServiceProvider,
    val mission: Mission,
    val name: String,
    val net: String,
    val pad: Pad,
    val probability: Any,
    val program: List<Any>,
    val rocket: Rocket,
    val slug: String,
    val status: Status,
    val url: String,
    val webcast_live: Boolean,
    val window_end: String,
    val window_start: String
)