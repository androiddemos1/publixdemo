package com.solutions.publixtest.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.solutions.publixtest.R
import com.solutions.publixtest.model.MainResponse
import com.solutions.publixtest.model.Result

class ListAdapter(private val dataSet: List<Result>?) : RecyclerView.Adapter<ListViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.result_item, parent, false)

        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        (holder.itemView.findViewById(R.id.launchName) as TextView).text =dataSet?.get(position)?.name
        (holder.itemView.findViewById(R.id.launchStatus) as TextView).text = dataSet?.get(position)?.status.toString()
    }

    override fun getItemCount(): Int {
        return dataSet?.size?:0
    }
}

class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

}