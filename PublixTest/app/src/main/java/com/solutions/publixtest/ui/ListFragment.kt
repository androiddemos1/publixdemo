package com.solutions.publixtest.ui

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.solutions.publixtest.R
import com.solutions.publixtest.network.Api
import com.solutions.publixtest.network.Repository

class ListFragment : Fragment() {

    companion object {
        fun newInstance() = ListFragment()
    }

    private lateinit var viewModel: ListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewModel = ViewModelProvider(this).get(ListViewModel::class.java)
        viewModel.getResults()
        viewModel.results.observe(viewLifecycleOwner, Observer {
           var rView :RecyclerView = (view.findViewById(R.id.recyclerview_results) as RecyclerView)
            rView.layoutManager = LinearLayoutManager(requireContext())
            rView.adapter = ListAdapter(viewModel.results.value)
            rView.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        })


    }

}