package com.solutions.publixtest.model

data class MainResponse(
    val count: Int,
    val next: String,
    val previous: Any,
    val results: List<Result>
)