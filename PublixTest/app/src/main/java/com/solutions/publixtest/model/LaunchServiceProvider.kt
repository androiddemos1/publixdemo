package com.solutions.publixtest.model

data class LaunchServiceProvider(
    val id: Int,
    val name: String,
    val type: String,
    val url: String
)