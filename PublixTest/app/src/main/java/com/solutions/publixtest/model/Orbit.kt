package com.solutions.publixtest.model

data class Orbit(
    val abbrev: String,
    val id: Int,
    val name: String
)