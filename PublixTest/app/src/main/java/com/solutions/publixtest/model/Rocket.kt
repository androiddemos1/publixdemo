package com.solutions.publixtest.model

data class Rocket(
    val configuration: Configuration,
    val id: Int
)